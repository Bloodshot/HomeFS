HomeFS
======


Description
-----------

HomeFS is a FUSE filesystem (written in Ruby) that directs filesystem calls
to a directory relative to the calling user's home directory. That is, if
the filesystem is mounted on `/mnt/homefs` and is linked to the directory
'Documents' relative to each user's home directory, the user Sarah doing
`ls /mnt/homefs/` would see the contents of `/home/sarah/Documents`, and
the user Tyler would see the contents of `/home/tyler/Documents`.

Since HomeFS only makes sense when it's used to display different contents
to different users, it by default passes the option 'allow_other' to FUSE,
which allows any user with the appropriate permissions to interact with the
filesystem.

Currently we do not support POSIX locking and creation of special device
files such as FIFOs (though HomeFS can read and write to and from special
device files). We do support extended filesystem attributes given that you
install the [ffi-xattr](https://rubygems.org/gems/ffi-xattr) gem, but you
can use HomeFS without it if you do not need them.


Installation
------------

HomeFS is available through https://rubygems.org and can be installed by
doing

    gem install homefs

Additionally, it may be built from the source at
[GitLab](https://gitlab.com/Bloodshot/HomeFS).


Usage
-----

    homefs relative_directory mountpoint [OPTIONS]

will mount HomeFS such that it displays `$HOME/relative_directory`.

Options may be standard FUSE filesystem options. See

    man mount.fuse

for a list.


Documentation
-------------

Library documentation is [here](http://www.rubydoc.info/gems/homefs),
but it's pretty minimal right now as we only have one class.


Why would I need this?
----------------------

The primary use I can see is to work around proprietary software that
insists its variable configuration files be stored in its install
directory (similar to how a lot of Windows software is packaged). To
package such software similar to how most Linux software should be
packaged, `/usr/share/enterprisesoftware/conf` could be a HomeFS mount that
points to `$HOME/.config/enterprisesoftware`. Furthermore, HomeFS can be
coupled with a unionfs of some sort that overlays static, or default,
configuration files with user defined configuration files.


Dependencies
------------

We depend on [rfuse](https://rubygems.org/gems/rfuse).

Optionally, we depend on [ffi-xattr](https://rubygems.org/gems/ffi-xattr)
for extended filesystem atribute support.


License
-------

[GPLv3](http://www.gnu.org/licenses/gpl-3.0.html)
