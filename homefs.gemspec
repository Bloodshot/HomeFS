Gem::Specification.new do |s|
    s.name        = 'homefs'
    s.version     = '0.4.0'
    s.date        = '2015-11-15'
    s.summary     = "FUSE Filesystem"
    s.description = "FUSE filesystem currently written in Ruby that directs
                    filesystem calls to a directory relative to the calling
                    user's home directory."
    s.authors     = ["Dylan Frese"]
    s.email       = 'dmfrese@gmail.com'
    s.files       = ["lib/homefs.rb", "lib/homefs/homefs.rb",
                     "lib/homefs/debug.rb", "man/homefs.1"]
    s.executables = ["homefs"]
    s.license     = 'GPLv2'
    s.add_runtime_dependency 'rfuse'
end
