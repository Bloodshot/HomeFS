class HomeFS
    class Wrapper
        def initialize(object)
            methods = (object.class.instance_methods -
                       Object.instance_methods)
            methods.each do |method|
                self.define_singleton_method(method) do |*args|
                    warn "#{method} called with args #{args.inspect}"
                    begin
                        ret = object.send(method, *args)
                        warn "\tMethod returned #{ret.inspect}"
                        return ret
                    rescue SystemCallError => e
                        warn e
                        raise e
                    rescue Exception => e
                        warn e
                        warn e.backtrace
                        raise e
                    end
                end
            end
        end
    end
end
