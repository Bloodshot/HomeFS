require 'rfuse'
require 'etc'

# An instance of HomeFS; this implements all FuseFS methods, and ultimately
# handles reading, writing, and inspection of files.
#
# We rely on the context provided to us by FUSE to determine the calling user's
# UID and GID.
#
# Currently, HomeFS does not support POSIX locking (which has little effect
# in practice), and special files (those created by mknod/FIFOs). We do
# supported extended filesystem attributes, but only if you have the
# 'ffi-xattr' gem.
# @author Dylan Frese
class HomeFS

    # Creates a new instance of HomeFS. This instance should probably be passed
    # to RFuse, as it does nothing on its own. The path is relative to the
    # calling user's home directory, that is, the user interacting with the
    # filesystem through open(2), read(2), write(2), etc.
    #
    # This method may accept a block. If a block is given, it is expected to
    # return the base path for the given UID. If no block is given, this is the
    # home directory for that UID instead.
    # @param [String] path the path, relative to $HOME, to use as the root of
    # the file system.
    # @yieldparam [Integer] uid the user ID of the current caller
    # @yieldreturn [String] the path to use as the base path for the given user
    def initialize(path, options = Hash.new, &blk)
        @relpath = path
        @baseblock = blk || proc do |uid|
            homedir(uid)
        end
    end

    # Get the home directory of the user with the UID _uid_.
    # @param [Integer] uid the UID of the user
    # @return [String] the path to the specified user's home directory
    def homedir(uid)
        Etc.getpwuid(uid).dir
    end

    # Return the path to the root of HomeFS relative to the underlying
    # filesystem. If _path_ is specified, it is taken to be relative to the
    # root of HomeFS.
    # @param [Integer] uid the UID whose home directory to use as a base
    # @param [String] path a relative path to a resource
    # @return [String] path to the root of the HomeFS relative to the underlying
    #   filesystem, or if _path_ is specified, the path to that resource
    #   relative to the underlying filesystem.
    def homepath(uid, path = nil)
        basepath = @baseblock[uid]
        # basepath shouldn't ever be nil, but fail gracefully just
        # in case.
        raise Errno::ENOENT if basepath == nil
        if path
            File.join(basepath, @relpath, path)
        else
            File.join(basepath, @relpath)
        end
    end

    # If HomeFS is running as root, drop_priv sets the effective user ID
    # and effective group ID to _uid_ and _gid_, respectively, and then
    # calls the block passed to the method. Before returning, drop_priv
    # that the EUID and EGID are each set back to 0.
    # @param [Integer] uid the user ID to set the effective user ID to
    # @param [Integer] gid the group ID to set the effective group ID to
    # @yield an environment with the EUID and EGID set to _uid_ and _gid_
    # @return the result of the block
    def drop_priv(uid, gid)
        if Process::Sys.getuid == 0
            begin
                Process::Sys.setegid(gid)
                Process::Sys.seteuid(uid)
                ret = yield
            ensure
                Process::Sys.seteuid(0)
                Process::Sys.setegid(0)
            end
        else
            ret = yield
        end
        return ret
    end

    # Check access permissions
    # @overload access(context,path,mode)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [Integer] mode the permissions to check
    # @return [void]
    # @raise [Errno::EACCESS] if the requested permission isn't available
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def access(context, path, mode)
        uid = context.uid
        hpath = homepath(uid, path)
        drop_priv(uid, context.gid) do
            access = true
            check_read = (mode & 4 != 0)
            check_write = (mode & 2 != 0)
            check_execute = (mode & 1 != 0)
            access &&= File.readable?(hpath) if check_read
            access &&= File.writable?(hpath) if check_write
            access &&= File.executable?(hpath) if check_execute
            access
        end
    end

    # Change file permissions
    # @overload chmod(context,path,mode)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [Integer] mode
    # @return [void]
    # @raise [Errno]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def chmod(context, path, mode)
        uid = context.uid
        hpath = homepath(uid, path)
        drop_priv(uid, context.gid) do
            File.chmod(mode, hpath)
        end
    end

    # Change file ownership
    # @overload chown(context,path,uid,gid)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [Integer] uid new user id
    # @param [Integer] gid new group id
    # @return [void]
    # @raise [Errno]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def chown(context, path, uid, gid)
        uid = context.uid
        hpath = homepath(uid, path)
        drop_priv(uid, context.gid) do
            File.chown(mode, hpath)
        end
    end

    # Create and open a file
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [Integer] mode the file permissions to create
    # @param [Fileinfo] ffi - use the FileInfo#fh attribute to store a
    #   filehandle
    # @return [void]
    # @raise [Errno]
    # If the file does not exist, first create it with the specified mode,
    #   and then open it.
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def create(context, path, mode, ffi)
        uid = context.uid
        hpath = homepath(uid, path)
        ffi.fh = drop_priv(uid, context.gid) do
            File.new(hpath, File::CREAT | File::WRONLY, mode)
        end
    end

    # Get attributes of an open file
    # @overload fgetattr(context,path,ffi)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [Fileinfo] ffi
    # @return [Stat] file attributes
    # @raise [Errno]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def fgetattr(context, path, ffi)
        ffi.fh.lstat
    end

    # Possibly flush cached data
    # @overload flush(context,path,ffi)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [FileInfo] ffi
    # @return [void]
    # @raise [Errno]
    # BIG NOTE: This is not equivalent to fsync(). It's not a request to sync dirty data.
    # Flush is called on each close() of a file descriptor. So if a
    # filesystem wants to return write errors in close() and the file has
    # cached dirty data, this is a good place to write back data and return
    # any errors. Since many applications ignore close() errors this is not
    # always useful.
    #
    # NOTE: The flush() method may be called more than once for each
    # open(). This happens if more than one file descriptor refers to an
    # opened file due to dup(), dup2() or fork() calls. It is not possible
    # to determine if a flush is final, so each flush should be treated
    # equally. Multiple write-flush sequences are relatively rare, so this
    # shouldn't be a problem.
    #
    # Filesystems shouldn't assume that flush will always be called after
    # some writes, or that if will be called at all.
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def flush(context, path, ffi) # We don't do any caching
    end

    # Synchronize file contents
    # @overload fsync(context,path,datasync,ffi)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [Integer] datasync if non-zero, then only user data should be
    #   flushed, not the metadata
    # @param [FileInfo] ffi
    # @return [void]
    # @raise [Errno]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def fsync(context, path, datasync, ffi)
        if datasync
            ffi.fh.fdatasync
        else
            ffi.fh.fsync
        end
    end

    # Change the size of an open file
    # @overload ftruncate(context,path,size,ffi)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [Integer] size
    # @param [Fileinfo] ffi
    # @return [void]
    # @raise [Errno]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def ftruncate(context, path, size, ffi)
        ffi.fh.truncate(size)
    end

    # Get file attributes.
    # @overload getattr(context,path)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @return [Stat] or something that quacks like a stat, or nil if the path does not exist
    # @raise [Errno]
    # Similar to stat(). The 'st_dev' and 'st_blksize' fields are ignored.
    # The 'st_ino' field is ignored except if the 'use_ino' mount option is
    # given.
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def getattr(context, path)
        uid = context.uid
        hpath = homepath(uid, path)
        drop_priv(uid, context.gid) do
            File.lstat(hpath)
        end
    end

    # Called when filesystem is initialised
    # @overload init(info)
    # @abstract
    # @param [Context] context
    # @param [Struct] info connection information
    # @return [void]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def init(context, info)
    end

    # Create a hard link to file
    # @overload link(context,from,to)
    # @abstract
    # @param [Context] context
    # @param [String] from
    # @param [String] to
    # @return [void]
    # @raise [Errno]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def link(context, from, to)
        uid = context.uid
        hfrom = homepath(uid, from)
        drop_priv(uid, context.gid) do
            File.link(hfrom, to)
        end
    end

    # (see RFuse::Fuse#mkdir)
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def mkdir(context, path, mode)
        uid = context.uid
        hpath = homepath(uid, path)
        drop_priv(uid, context.gid) do
            Dir.mkdir(hpath, mode)
        end
    end

    # File open operation
    # @overload open(context,path,ffi)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [FileInfo] ffi
    # file open flags etc.
    # The fh attribute may be used to store an arbitrary filehandle object
    # which will be passed to all subsequent operations on this file
    # @raise [Errno::ENOPERM] if user is not permitted to open the file
    # @raise [Errno] for other errors
    # @return [void]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def open(context, path, ffi)
        uid = context.uid
        hpath = homepath(uid, path)

        # We pass the flags straight into File.open because the constants
        # in RFuse::Fcntl and File::Constants have the same values, because
        # they both have the same values as the open syscall. If this were
        # not the case, we'd have the map the values of RFuse::Fcntl to
        # their equivalents in File::Constants
        drop_priv(uid, context.gid) do
            ffi.fh = File.open(hpath, ffi.flags)
        end
    end

    # Open directory
    # @overload opendir(context,path,name)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [FileInfo] ffi
    # @return [void]
    # @raise [Errno]
    # Unless the 'default_permissions' mount option is given, this method
    # should check if opendir is permitted for this directory. Optionally
    # opendir may also return an arbitrary filehandle in the fuse_file_info
    # structure, which will be available to {#readdir},  {#fsyncdir},
    # {#releasedir}.
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def opendir(context, path, ffi)
        uid = context.uid
        hpath = homepath(uid, path)
        ffi.fh = drop_priv(uid, context.gid) do
            Dir.new(hpath)
        end
    end

    # Read data from an open file
    # @overload read(context,path,size,offset,ffi)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [Integer] size
    # @param [Integer] offset
    # @param [FileInfo] ffi
    # @return [String] should be exactly the number of bytes requested, or
    #   empty string on EOF
    # @raise [Errno]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def read(context, path, size, offset, ffi)
        if offset < 0
            ffi.fh.seek(offset, :END)
        else
            ffi.fh.seek(offset, :SET)
        end
        ffi.fh.read(size) || ''
    end

    # (see RFuse::Fuse#readdir)
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def readdir(context, path, filler, offset, ffi)
        filler.push(".", nil, 0)
        filler.push("..", nil, 0)
        ffi.fh.pos = offset
        ffi.fh.each do |filename|
            next if filename == '.' || filename == '..'
            filler.push(filename, nil, 0)
        end
    end

    # Resolve target of symbolic link
    # @overload readlink(context,path,size)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [Integer] size if the resolved path is greater than this size
    #   it should be truncated
    # @return [String] the resolved link path
    # @raise [Errno]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def readlink(context, path, size)
        # Is it okay that we return the 'real' path here?
        File.readlink(homepath(context.uid, path))[0 ... size]
    end

    # Release an open file
    # @overload release(context,path,ffi)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [FileInfo] ffi
    # @return [void]
    # Release is called when there are no more references to an open file:
    # all file descriptors are closed and all memory mappings are unmapped.
    #
    # For every {#open} call there will be exactly one {#release} call with
    # the same flags and file descriptor. It is possible to have a file
    # opened more than once, in which case only the last release will mean,
    # that no more reads/writes will happen on the file.
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def release(context, path, ffi)
        ffi.fh.close
    end

    # (see RFuse::Fuse#rename)
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def rename(context, from, to)
        uid = context.uid
        hfrom = homepath(uid, from)
        hto = homepath(uid, to)
        drop_priv(uid, context.gid) do
            FileUtils.mv(hfrom, hto, :force => true)
        end
    end

    # Create a symbolic link
    # @overload symlink(context,to,from)
    # @abstract
    # @param [Context] context
    # @param [String] to
    # @param [String] from
    # @return [void]
    # @raise [Errno]
    # Create a symbolic link named "from" which, when evaluated, will lead
    #   to "to".
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def symlink(context, to, from)
        uid = context.uid
        hfrom = homepath(uid, from)
        drop_priv(uid, context.gid) do
            File.symlink(to, hfrom)
        end
    end

    # Change the size of a file
    # @overload truncate(context,path,offset)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [Integer] offset
    # @return [void]
    # @raise [Errno]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def truncate(context, path, offset)
        uid = context.uid
        hpath = homepath(uid, path)
        drop_priv(uid, context.gid) do
            File.truncate(hpath, offset)
        end
    end

    # Remove a file
    # @overload unlink(context,path)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @return [void]
    # @raise [Errno]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def unlink(context, path)
        uid = context.uid
        hpath = homepath(uid, path)
        drop_priv(uid, context.gid) do
            File.unlink(hpath)
        end
    end

    # Change access/modification times of a file
    # @overload utimens(context,path,actime,modtime)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [Integer] actime access time in nanoseconds
    # @param [Integer] modtime modification time in nanoseconds
    # @return [void]
    # @raise [Errno]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def utimens(context, path, actime, modtime)
        uid = context.uid
        hpath = homepath(uid, path)
        drop_priv(uid, context.gid) do
            File.utime(actime, modtime, hpath)
        end
    end

    # Write data to an open file
    # @overload write(context,path,data,offset,ffi)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [String] data
    # @param [Integer] offset
    # @param [FileInfo] ffi
    # @return [Integer] exactly the number of bytes requested except on
    #   error
    # @raise [Errno]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    def write(context, path, data, offset, ffi)
        if offset < 0
            ffi.fh.seek(offset, :END)
        else
            ffi.fh.seek(offset, :SET)
        end
        ffi.fh.write(data)
    end

begin

    require 'ffi-xattr'

    # Get extended attribute
    # @overload getxattr(context,path,name)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [String] name
    # @return [String] attribute value
    # @raise [Errno] Errno::ENOATTR if attribute does not exist
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    # @note This method is only defined if the gem 'ffi-xattr' is
    # available
    def getxattr(context, path, name)
        uid = context.uid
        hpath = homepath(uid, path)
        drop_priv(uid, context.gid) do
            check_nodata Xattr::Lib.get(hpath, false, name)
        end
    end

    # Set extended attributes
    # @overload setxattr(context,path,name,data,flags)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [String] name
    # @param [String] data
    # @param [Integer] flags
    # @return [void]
    # @raise [Errno]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    # @note This method is only defined if the gem 'ffi-xattr' is
    # available
    def setxattr(context, path, name, data, flags)
        uid = context.uid
        hpath = homepath(uid, path)
        drop_priv(uid, context.gid) do
            Xattr::Lib.set(hpath, false, name, data)
        end
    end

    # List extended attributes
    # @overload listxattr(context,path)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @return [Array<String>] list of attribute names
    # @raise [Errno]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    # @note This method is only defined if the gem 'ffi-xattr' is
    # available
    def listxattr(context, path)
        uid = context.uid
        hpath = homepath(uid, path)
        drop_priv(uid, context.gid) do
            check_nodata Xattr::Lib.list(hpath, false)
        end
    end

    # Remove extended attribute
    # @overload removexattr(context,path,name)
    # @abstract
    # @param [Context] context
    # @param [String] path
    # @param [String] name attribute to remove
    # @return [void]
    # @raise [Errno]
    # @note This method should usually only be called by FUSE, and not
    #   called directly
    # @note This method is only defined if the gem 'ffi-xattr' is
    #   available
    def removexattr(context, path, name)
        check_writable(context.uid, path)
        hpath = homepath(context.uid, path)
        Xattr::Lib.remove(hpath, false, name)
    end

    # Raise Errno::ENODATA if _value_ is nil. Otherwise, return _value_.
    # @param [Object] value the value to check
    # @return [Object] the given value
    # @raise [Errno::ENODATA] if value is nil
    def check_nodata(value)
        if value.nil?
            fail Errno::ENODATA
        end
        value
    end
    private :check_nodata

rescue LoadError # require 'ffi-xattr'
    # We don't have the library, so we don't define those methods
end

end
